import { Button, StyleSheet, Switch, Text, View } from "react-native";

const TodoListItem = ({item, onItemChange, onItemDelete}) => {
  const onDoneChange = newValue => {
    item.done = newValue;
    onItemChange(item);
  };

  return (
    <View style={{"flexDirection": "row-reverse"}}>
      <Button
        onPress={() => onItemDelete(item)}
        title="delete"
        color="#841584"
        style={{"margin": 10}}
      />
      <Switch value={item.done} onValueChange={onDoneChange} />
      <Text style={[styles.text, item.done && styles.textStrikethrough,{"flex": 1}]}>{item.task}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  text: {
    padding: 10,
    fontSize: 18,
    height: 44,
  },
  textStrikethrough: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },

});

export default TodoListItem;
