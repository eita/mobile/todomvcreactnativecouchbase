/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import type {Node} from 'react';
import React, {useEffect, useState} from 'react';
import {
  Alert,
  Button,
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  ToastAndroid,
  useColorScheme,
  View,
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import TodoListItem from './components/TodoListItem';
import * as Cblite from 'react-native-cblite';
import * as RNFS from 'react-native-fs';
import {deleteDocument, setDocument} from 'react-native-cblite';

const CouchbaseNativeModule = Cblite;
const showToast = message => {
  ToastAndroid.show('Message:' + message, ToastAndroid.SHORT);
};

const Separator = () => <View style={styles.separator} />;

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [currentTodo, setCurrentTodo] = useState('');
  const [todoList, setTodoList] = useState([]);

  const dbName = 'todomvc_db';

  const userId = 'user1';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  //Gets list of tasks from database

  const loadTasksFromDb = (
    successCallback = () => {},
    errorCallback = () => {},
  ) => {
    CouchbaseNativeModule.query(
      dbName,
      'SELECT META().id AS internal_id, id, task, type, done FROM _ WHERE type = "task"',
      successResult => {
        console.log('success retrieving');
        console.log(successResult.toString());
        successCallback(JSON.parse(successResult));
      },
      errorResult => {
        console.log(errorResult.toString());
      },
    );
  };

  const saveTaskToDb = (
    task,
    successCallback = () => {},
    errorCallback = error => {},
  ) => {
    const taskId = task.id || 'task_' + userId + '_' + Date.now();
    task.id = taskId;

    setDocument(
      dbName,
      taskId,
      JSON.stringify(task),
      result => {
        if (result == 'Success') {
          successCallback();
        } else {
          console.log('setDocument error: ' + result);
          errorCallback(result);
        }
      },
      error => {
        console.log(JSON.stringify(error.userInfo));
        console.log(JSON.stringify(error));
        errorCallback(JSON.stringify(error.userInfo));
      },
    );
  };

  const deleteTaskFromDb = (
    task,
    successCallback = () => {},
    errorCallback = error => {},
  ) => {
    if (task.id == null) {
    }
    const taskId = task.id || 'task_' + userId + '_' + Date.now();

    deleteDocument(
      dbName,
      task.id,
      result => {
        if (result == 'Success') {
          successCallback();
        } else {
          console.log('deleteDocument error: ' + result);
          errorCallback(result);
        }
      },
      error => {
        console.log(JSON.stringify(error.userInfo));
        console.log(JSON.stringify(error));
        errorCallback(JSON.stringify(error.userInfo));
      },
    );
  };

  //runs in the first time
  useEffect(() => {
    const loggingResponse = CouchbaseNativeModule.enableConsoleLogging(
      'replicator',
      'verbose',
    );
    console.log('Enable Logging :', loggingResponse);

    // start couchbase db
    let _directory = RNFS.CachesDirectoryPath + '/' + 'todomvc';

    let config = {Directory: _directory};
    CouchbaseNativeModule.CreateOrOpenDatabase(
      dbName,
      config,
      successResponse => {
        console.log(successResponse);

        if (
          successResponse == 'Success' ||
          successResponse == 'Database already exists'
        ) {
          console.log('database ' + successResponse);
          //login successful
        } else {
          alert('There was a problem while login.');
        }
      },
      errorResponse => {
        console.log(errorResponse);
        alert('There was a problem while login : ' + errorResponse);
      },
    );

    loadTasksFromDb(
      tasks => {
        setTodoList(tasks);
      },
      errorResponse => {
        console.log(errorResponse);
      },
    );
  }, []);

  const onTodoItemChange = todoItemChanged => {
    saveTaskToDb(
      todoItemChanged,
      () => {
        console.log('success updating');
        const newTodoList = todoList.map((element, index) => {
          if (element.id == todoItemChanged.id) {
            return todoItemChanged;
          } else {
            return element;
          }
        });
        setTodoList(newTodoList);
      },
      errorResult => {
        ToastAndroid.show('Error updating task: ' + errorResult);
        console.log('error updating');
      },
    );
  };

  const onTodoItemDelete = todoItemDeleted => {
    Alert.alert(
      'Delete Item',
      'Do you want to delete item ' + todoItemDeleted.task + '?',
      [
        {
          text: 'Cancel',
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            console.log('OK Pressed');
            deleteTaskFromDb(
              todoItemDeleted,
              () => {
                console.log('success deleting');
                const newTodoList = todoList.filter(
                  a => a.id !== todoItemDeleted.id,
                );
                setTodoList(newTodoList);
              },
              errorResult => {
                ToastAndroid.show('Error deleting task: ' + errorResult);
                console.log('error deleting');
              },
            );
          },
        },
      ],
    );
  };

  const onPressAddTask = taskDescription => {
    if (taskDescription == null || taskDescription.trim() == '') {
      console.log('Empty task');
      ToastAndroid.show('Empty task', ToastAndroid.SHORT);
    } else {
      console.log(taskDescription);
      const newTask = {
        task: taskDescription,
        done: false,
        type: 'task',
      };

      console.log('before saving');
      saveTaskToDb(
        newTask,
        () => {
          console.log('success saving');
          const newTodoList = todoList.concat([newTask]);
          setTodoList(newTodoList);
          setCurrentTodo('');
        },
        errorResult => {
          ToastAndroid.show('Error adding task: ' + errorResult);
          console.log('error saving');
        },
      );
    }
  };

  const startListening = () => {};

  const stopListening = () => {};

  const startReplicator = () => {};

  const stopReplicator = () => {};

  return (
    <SafeAreaView style={backgroundStyle}>
      <Text style={styles.header}>TodoMvc App</Text>
      <FlatList
        data={todoList}
        renderItem={({item}) => (
          <TodoListItem
            item={item}
            onItemChange={onTodoItemChange}
            onItemDelete={onTodoItemDelete}
          />
        )}
      />
      <TextInput
        style={styles.input}
        onChangeText={inp => setCurrentTodo(inp)}
        value={currentTodo}
        placeholder="describe the next task here"
        keyboardType="default"
        multiline
        numberOfLines={2}
      />
      <Button
        onPress={() => onPressAddTask(currentTodo)}
        title="add task"
        color="#841584"
        style={styles.button}
      />
      <Separator />
      <Button
        onPress={() => startListening()}
        title="start sync listener"
        color="#841584"
        style={styles.button}
      />
      <Separator />
      <Button
        onPress={() => stopListening()}
        title="stop sync listener"
        color="#841584"
        style={styles.button}
      />
      <Separator />
      <Button
        onPress={() => startReplicator()}
        title="start sync replicator"
        color="#841584"
        style={styles.button}
      />
      <Separator />
      <Button
        onPress={() => stopReplicator()}
        title="stop sync replicator"
        color="#841584"
        style={styles.button}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  item: {
    padding: 10,
    fontSize: 18,
    height: 44,
    color: 'black',
  },
  header: {
    padding: 10,
    fontSize: 36,
    fontWeight: 'bold',
  },

  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },

  button: {
    margin: 10,
  },

  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
});

export default App;
